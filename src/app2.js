import '@tarojs/async-await'
import Taro, { Component } from '@tarojs/taro'
import { Provider } from '@tarojs/redux'
// import { USER } from './constants/storage_keys'
import { isIphoneX } from './util/js/tool'
import Index from './pages/index'
import { post } from './util/js/request'
import configStore from './store'
import { CHANGE_USER } from './constants/user'
import { USER } from './constants/storage_keys'

import './app.scss'

// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }
const store = configStore()

class App extends Component {

  config = {
    pages: [
      'pages/index/index',
      'pages/login/login',
      'pages/mobile/mobile',
      'pages/password/password',
      'pages/mall/mall',
      'pages/message/message',
      'pages/recharge/recharge',
      'pages/my/my',
      'pages/my_vm_detail/my_vm_detail',
      'pages/vm_info/vm_info',
      'pages/mall_detail/mall_detail',
      'pages/about/about',
      'pages/record/record',
      'pages/help/help',
      'pages/weixin/weixin',
      'pages/feedback/feedback'
    ],
    window: {
      // navigationStyle: 'custom',
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: '翱游',
      navigationBarTextStyle: 'black'
    },
    tabBar: {
      color: '#ffffff',
      selectedColor: '#4CCCC5',
      backgroundColor: '#202833',
      list: [
        {
          iconPath: './assets/images/my_normal@3x.png',
          selectedIconPath: './assets/images/my_select@3x.png',
          'pagePath': 'pages/index/index',
          'text': '我的主机'
        },
        {
          iconPath: './assets/images/mall_list_normal@3x.png',
          selectedIconPath: './assets/images/mall_list_select@3x.png',
          'pagePath': 'pages/mall/mall',
          'text': '主机商城'
        },
        {
          iconPath: './assets/images/recharge_normal@3x.png',
          selectedIconPath: './assets/images/recharge_select@3x.png',
          'pagePath': 'pages/recharge/recharge',
          'text': '水晶充值'
        },
        {
          iconPath: './assets/images/message_normal@3x.png',
          selectedIconPath: './assets/images/message_select@3x.png',
          'pagePath': 'pages/message/message',
          'text': '我的消息'
        },
        {
          iconPath: './assets/images/person_normal@3x.png',
          selectedIconPath: './assets/images/person_select@3x.png',
          'pagePath': 'pages/my/my',
          'text': '个人中心'
        }
      ]
    }
  }
  globalData = {
    refreshUser: async () => {
      let userInfo = await post('user/userInfo/get', { xx: new Date().getTime()})
      if (!userInfo || userInfo.success !== 'true') return
      let info =  null
      if (global.cookie.get(USER)) {
        info = JSON.parse(decodeURIComponent(global.cookie.get(USER)))
      } else {
        info = Taro.getStorageSync(USER) || {}
      }
      Object.assign(info, userInfo.info)
      store.dispatch(
        {
          type: CHANGE_USER,
          data: info
        }
      )
    },
    getVmState: async (type, vmid) => {
      return new Promise(async resolve => {
        let b = true
        let res = await post('/vm/stateVm/get', {
          type,
          vmid
        })
        if (res.success !== 'true') {
          Taro.showToast({
            icon: 'none',
            title: res.msg
          })
          b = false
          this.globalData.addConnectLog(type - 1, vmid, res.msg, 1, res.code)
        }
        if (res.info.availtime <= 0) {
          Taro.showToast({
            icon: 'none',
            title: res.info.lesstimemsg
          })
          b = false
        }
        if (res.info.vm_state * 1 === 1 || res.info.vm_state * 1 === 2) {
          Taro.showToast({
            icon: 'none',
            title: `主机${['重置', '重启'][res.info.vm_state - 1]}中`
          })
          return false
        }
        resolve(b)
      })
    },
    addConnectLog: (type, vmid, resultBody, action = 1, code) => {
      post('vm/operRecord/post', {
        type,
        vmid,
        action,
        code,
        resultBody
      })
    }
  }

  componentDidMount () {
    // if (process.env.NODE_ENV !== 'production' && Taro.getSystemInfoSync().platform !== 'devtools') {
    //   Taro.setEnableDebug({
    //     enableDebug: true
    //   })
    // }
    // Taro.setEnableDebug({
    //   enableDebug: true
    // })
  }

  componentDidShow () {
    if (process.env.TARO_ENV === 'h5' && isIphoneX()) {
      document.getElementsByClassName('taro-tabbar__tabbar')[0].className += ' iphone-x-bottom'
    }
  }

  componentDidHide () {}

  componentDidCatchError () {}

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render () {
    return (
      <Provider store={store}>
        <Index />
      </Provider>
    )
  }
}

Taro.render(<App />, document.getElementById('app'))
