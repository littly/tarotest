/*global preval */
import { Provider } from '@tarojs/redux'
// import { Provider } from '@tarojs/mobx'
// import { observable } from 'mobx'
import Taro, { Config } from '@tarojs/taro'

import Index from './pages/index/index'
import configStore from './store'

import './app.scss'
import './style/base.scss'

const store = configStore()
// const store = observable({
//   indexStore: "Noa"
// })

const navigateTo = Taro.navigateTo
Taro.navigateTo = (...args) => {
  console.log('test')
  navigateTo(...args)
}


/**
 *  入口
 */
class App extends Taro.Component {
  constructor(...args) {
    super(...args)
  }
  config: Config = {
    pages: preval`
      module.exports = [
        'pages/index/index',
        'pages/issue/4959',
        'pages/about/about',
        'pages/input/input',
        'pages/goods/goods',
        'pages/tabbar/tabbar',
        'pages/navigateTest/navigateTest',
        'pages/actionSheet/actionSheet',
        'pages/scrollView/scrollView',
        'pages/scrollView/withoutComponent',
        'pages/scrollView/renderWithMap',
        'pages/canvas/canvas',
        'pages/video/video',
        'pages/audio/audio',
        'pages/location/location',
        'pages/accelerometer/accelerometer',
        'pages/image/image',
        'pages/files/files',
        'pages/routers/routers',
        'pages/compass/compass',
        'pages/modal/modal',
        'pages/mobx/mobx',
        'pages/resolve/npm_files'
      ]
   `,
    subPackages: [{
      root: "packagea",
      pages: [
        'pages/index/index'
      ]
    }],
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: '互联网+小程序平台',
      navigationBarTextStyle: 'black'
    },
    tabBar: {
      color: '#333',
      selectedColor: '#409EFF',
      backgroundColor: '#fff',
      borderStyle: 'black',
      list: [{
        pagePath: "pages/index/index",
        text: "首页"
      }, {
        pagePath: "pages/tabbar/tabbar",
        text: "tabbar"
      }, {
        pagePath: "pages/about/about",
        text: "关于我们"
      }]
    }
  }
  componentWillMount() {
    // console.log(this.$router)
  }

  /**
   * 显示
   */
  componentDidMount() {
  }

  componentDidShow() {
    console.log('app didshow')
  }

  componentDidHide() {
  }

  componentCatchError() {

  }

  render() {
    return (
      <Provider store={store}>
        <Index />
      </Provider>
    )
  }
}

Taro.render(<App />, document.getElementById('app'));
