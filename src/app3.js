import './app.css'

import Taro from '@tarojs/taro'
import withWeapp from '@tarojs/with-weapp'

@withWeapp()
class App extends Taro.Component {
  componentWillMount = () => {
    // 展示本地存储能力
    var logs = Taro.getStorageSync('logs') || []
    logs.unshift(Date.now())
    Taro.setStorageSync('logs', logs)

    // 登录
    Taro.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
    // 获取用户信息
    Taro.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          Taro.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  }
  globalData = {
    userInfo: null,

    httpurl_buy:
      'https://www.s7-airlines.com/home/personal_account/registration.dot',
    httpurl_guanli: 'https://www.s7-airlines.com/myb/en/',
    httpurl_zhiji: 'https://www.s7-airlines.com/webcheckin/en/',
    httpurl_hangban:
      'https://www.s7-airlines.com/zh/info/tablo?searchType=flight_number'
  }
  config = {
    pages: [
      // 'pages/logs/logs',
      'pages/index/index',
      // 'pages/Hangban/Hangban',
      // 'pages/maipiao/maipiao',
      // 'pages/guanli/guanli',
      // 'pages/zhiji/zhiji'
    ],
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#00649E',
      navigationBarTextStyle: 'white',
      navigationBarTitleText: 'WeChat'
    },
    sitemapLocation: 'sitemap.json'
  }

  componentWillMount() {
    this.$app.globalData = this.globalData
  }

  render() {
    return null
  }
} //app.js

Taro.render(<App />, document.getElementById('app'))
export default App