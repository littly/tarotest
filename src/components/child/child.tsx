import Nerv from 'nervjs'
import { Component } from '@tarojs/taro';

class Child extends Component<{ onTest, renderHeader }> {
  componentDidMount () {
    this.props.onTest('onTest')
  }
  render () {
    return <div>
      {this.props.renderHeader}
    </div>
  }
}

export default Child;
