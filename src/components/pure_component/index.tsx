import Taro, { PureComponent } from '@tarojs/taro'
import { Text, View } from '@tarojs/components'

export default class Comp extends PureComponent {
  render () {
    return (
      <View>
        <Text>PureComponent</Text>
      </View>
    )
  }
}
