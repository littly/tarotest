import { Button } from '@tarojs/components'
import Btn from './ParentBtn'


export default class WrapButton extends Btn {
  // 此处如果注释了，父类constructor中的console将不会执行
  // 如果不注释，则报错: Uncaught TypeError: Cannot read property 'store' of undefined （redux里的context为空）
  constructor(props, context) {
    super(props, context)
  }
  render() {
    // console.log('wrapButton props', this.props)
    const { count } = this.props 
    super.render()
    return (
      <Button>{count}</Button>
    )
  }
}