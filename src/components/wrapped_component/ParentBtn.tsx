import { Component } from '@tarojs/taro'
import { connect } from '@tarojs/redux'

const mapStateToProps = ({counter}) => {
  return { count: counter.num }
}

@connect(mapStateToProps, null)
export default class Btn extends Component<{
  count
}> {
  constructor(props, context) {
    // 不会打印Btn
    console.log('Btn')
    super(props, context)
  }
}