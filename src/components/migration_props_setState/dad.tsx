import Taro, { Component } from '@tarojs/taro';

export default class Index extends Component {
  config = {};
  state = {
    currentKey: '0',
    isShow: false,
    currentChannel: {},
    channelsList: []
  }

  componentWillMount() {}

  componentWillUnmount() {}

  componentDidShow() {}

  componentDidHide() {}

  // onShareAppMessage() {
  //     return sharePage(currentPath, this.$router.params);
  // }
  getTargetData() {
    const currentChannel = this.state.channelsList[parseInt(this.state.currentKey, 10)] || {};
    this.setState({
      currentChannel
    });
  }

  selectNav(key) {
    this.setState(
      {
        currentKey: key
      },
      this.getTargetData
    );
  }
  render() {}
}
