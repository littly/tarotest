import { View, Text } from '@tarojs/components';
import Taro, { Component } from '@tarojs/taro';

export default class HomeNav extends Component<{
  onClick
}> {
  state = {
    imgList: [
      {
        active: 'beio-active-new.png',
        unactive: 'beio-no-active-new.png',
        key: '0',
        activeCls: 'beio-a',
        unActiveCls: 'beio',
      },
      {
        active: 'omei-active-new.png',
        unactive: 'omei-no-active-new.png',
        key: '1',
        cls: 'nav nav-omei',
        activeCls: 'omei-a',
        unActiveCls: 'omei',
      },
      {
        active: 'xian-active-new.png',
        unactive: 'xian-no-active-new.png',
        key: '2',
        activeCls: 'xian-a',
        unActiveCls: 'xian',
      },
      {
        active: 'zhong-active-new.png',
        unactive: 'zhong-no-active-new.png',
        key: '3',
        activeCls: 'zhong-a',
        unActiveCls: 'zhong',
      },
    ],
    activeKey: '0',
    isNeedImgBase: true,
    mode: 'scaleToFill',
  }
  toSearchPage() {
    Taro.navigateTo({
      url: '/pages/search/index'
    })
  }
  selectNav(key) {
    this.setState({
      activeKey: key,
    })
    this.props.onClick(key)
  }
  render() {
    const { imgList, activeKey } = this.state
    const imgElemList = imgList.map((img) => {
      let cls = 'nav-img'
      if (activeKey === img.key) {
        cls = `${cls} ${img.activeCls}`
      } else {
        cls = `${cls} ${img.unActiveCls}`
      }
      return <View className='nav' key={img.key} onClick={this.selectNav.bind(this, img.key)}>
        <View className={cls}>
          <Text>Hello</Text>
        </View>
      </View>
    })

    return (
      <View className='home-nav-cont'>
        <View className='search' onClick={this.toSearchPage}>
          <View className='icon'></View>
          <View className='split'></View>
        </View>
        <View className='nav-ct'>
          {imgElemList}
        </View>
      </View>
    )
  }
}