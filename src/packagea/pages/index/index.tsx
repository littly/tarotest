import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Button, Text } from '@tarojs/components'
import PureComp from '../../../components/pure_component'

// import { add, minus, asyncAdd } from '../../../actions/counter'

import './index.css'

// #region 书写注意
// 
// 目前 typescript 版本还无法在装饰器模式下将 Props 注入到 Taro.Component 中的 props 属性
// 需要显示声明 connect 的参数类型并通过 interface 的方式指定 Taro.Component 子类的 props
// 这样才能完成类型检查和 IDE 的自动提示
// 使用函数模式则无此限制
// ref: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/20796
//
// #endregion

type PageStateProps = {
  counter: {
    num: number
  }
}

type PageDispatchProps = {
  add: () => void
  dec: () => void
  asyncAdd: () => any
}

type PageOwnProps = {}

type PageState = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface Package {
  props: IProps;
}

// @connect(({ counter }) => ({
//   counter
// }), (dispatch) => ({
//   add () {
//     dispatch(add())
//   },
//   dec () {
//     dispatch(minus())
//   },
//   asyncAdd () {
//     dispatch(asyncAdd())
//   }
// }))
class Package extends Component {

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '分包的首页'
  }

  isExperimental = true

  componentWillMount () {
    console.log(this.$router)
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} willMount`)
  }

  componentDidMount () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} didMount`)
  }

  componentDidShow () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} didShow`)
    Taro.showModal({
      title: '提示',
      content: '内容',
      success: res => {
        console.log(res, 'clicked')
      }
    })
  }

  componentWillReceiveProps () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} willReveiveProps`)
  }

  componentWillUnmount () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} willUnmount`)
  }

  componentDidHide () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} didHide`)
  }

  navigate () {
    Taro.navigateTo({
      url: '/pages/index/index?type=test'
    })
  }
  navigateBack = () => {
    Taro.navigateBack({
      delta: 1
    })
  }

  render () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} render`)
    return (
      <View className='packaega'>
        <Text>page 3</Text>
        <Button onClick={this.navigate}>navigate</Button>
        <Button onClick={this.navigateBack}>back</Button>
        <Button className='add_btn' onClick={this.props.add}>+</Button>
        <Button className='dec_btn' onClick={this.props.dec}>-</Button>
        <Button className='dec_btn' onClick={this.props.asyncAdd}>async</Button>
        <View><Text>Hello, subindex</Text></View>
        <View><Text><pre>$router: {JSON.stringify(this.$router, null, 2)}</pre></Text></View>
        <PureComp />
      </View>
    )
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default Package as ComponentClass<PageOwnProps, PageState>
