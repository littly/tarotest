import Taro from '@tarojs/taro'
import { View, Navigator, Button } from '@tarojs/components'

class ImagePage extends Taro.Component {
  upload = () => {
    Taro.chooseImage({})
      .then(res => {
        Taro.uploadFile({
          url: 'xx',
          filePath: res.tempFilePaths[0],
          name: 'xx',
        })
      })
  }
  download = () => {
    Taro.downloadFile({
      url: 'http://www.baidu.com/bd_logo1.png'
    })
  }
  render () {
    return (
      <View>
        <Navigator openType='navigateBack' delta={1}>back</Navigator>
        <Button onClick={this.upload}>上传</Button>
        <Button onClick={this.download}>下载</Button>
      </View>
    )
  }
}

export default ImagePage
