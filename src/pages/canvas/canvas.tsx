import { Canvas, View, Text, Button, Navigator } from '@tarojs/components'
import Taro, { Component } from '@tarojs/taro'
import './canvas.css'

function ball(context, x, y) {
  context.beginPath(0)
  context.arc(x, y, 5, 0, Math.PI * 2)
  context.fillStyle = '#1aad19'
  context.strokeStyle = 'rgba(1,1,1,0)'
  context.fill()
  context.stroke()
}

export default class CanvasPage extends Component {
  position = {
    x: 150,
    y: 150,
    vx: 2,
    vy: 2
  }
  state = {
    touchX: 0,
    touchY: 0,
    width: 305,
    height: 305
  }
  timer: number | null = null
  draw (context) {
    const p = this.position
    p.x += p.vx
    p.y += p.vy
    if (p.x >= 300) {
      p.vx = -2
    }
    if (p.x <= 7) {
      p.vx = 2
    }
    if (p.y >= 300) {
      p.vy = -2
    }
    if (p.y <= 7) {
      p.vy = 2
    }
    
    context.beginPath()
    context.arc(p.x, p.y, 5, 0, Math.PI * 2)
    context.fillStyle = '#1aad19'
    context.strokeStyle = 'rgba(1,1,1,0)'
    context.fill()
    context.stroke()
    ball(context, p.x, 150)
    ball(context, 150, p.y)
    ball(context, 300 - p.x, 150)
    ball(context, 150, 300 - p.y)
    ball(context, p.x, p.y)
    ball(context, 300 - p.x, 300 - p.y)
    ball(context, p.x, 300 - p.y)
    ball(context, 300 - p.x, p.y)
    context.draw()
  }
  onTouchMove = e => {
    this.setState({
      touchX: e.touches[0].x,
      touchY: e.touches[0].y
    })
  }
  onTouchEnd = () => {
    this.setState({
      touchX: 0,
      touchY: 0
    })
  }
  __imageData
  getImageData = () => {
    Taro.canvasGetImageData({
      canvasId: 'canvas',
      x: 0,
      y: 0,
      width: 300,
      height: 300
    }).then(res => {
      console.log(res)
      this.__imageData = res.data
    })
  }
  putImageData = () => {
    Taro.canvasPutImageData({
      canvasId: 'canvas',
      x: 0,
      y: 0,
      width: 300,
      height: 300,
      data: this.__imageData
    }).then(res => {
      console.log(res)
    })
  }
  toTempFilePath = () => {
    // Taro.canvasToTempFilePath({
    //   canvasId: 'canvas'
    // }).then(res => {
    //   console.log(res)
    // })
  }
  cnt = 0
  setSize = () => {
    const nCnt = this.cnt++
    this.setState({
      width: nCnt % 2 ? 305 : 155,
      height: nCnt % 2 ? 305 : 155
    })
  }
  componentDidMount () {
    const context = Taro.createCanvasContext('canvas', this)
    console.log(context.measureText('text'))

    this.timer = setInterval(() => {
      this.draw(context)
    }, 17)
  }
  componentWillUnmount () {
    this.timer && clearInterval(this.timer)
  }
  render () {
    return (
      <View>
        <Navigator openType='navigateBack' delta={1}>back</Navigator>
        <Canvas
          className={'test_canvas'}
          canvasId='canvas'
          style={{
            width: this.state.width,
            height: this.state.height
          }}
          onTouchMove={this.onTouchMove}
          onTouchEnd={this.onTouchEnd}
          />
        <Text>touchpos: ({this.state.touchX}, {this.state.touchY})</Text>
        <Button onClick={this.getImageData}>getImageData</Button>
        <Button onClick={this.putImageData}>putImageData</Button>
        <Button onClick={this.toTempFilePath}>toTempFilePath</Button>
        <Button onClick={this.setSize}>setSize</Button>
      </View>
    )
  }
}
