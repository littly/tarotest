import { Button, Image, Navigator, Text, View } from '@tarojs/components'
import Taro from '@tarojs/taro'
import { ComponentClass } from 'react'

import Child from '@components/child/child'
import ReduxBtn from '@components/nested_redux/ReduxView'
import PureComponent from '@components/pure_component/reflect'
import img from '@/src/static/aaa.jpeg'
import FuncComp from '@components/func'
import PlatformSpecificComp from '@components/platformSpecificComp/comp'

import './index.scss'
import './index.styl'
// import './iconfont.css'

import Test from '../../test'

const animation = Taro.createAnimation({
  transformOrigin: "50% 50%",
  duration: 1000,
  timingFunction: "ease",
  delay: 0
})
animation.export()
console.log(Test)

type PageStateProps = {
  counter: {
    num: number
  }
}

type PageDispatchProps = {
  add: () => void
  dec: () => void
  asyncAdd: () => any
}

type PageOwnProps = {}

type PageState = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface Index {
  props: IProps;
}

class Dad extends Taro.Component {
  componentDidShow () {
    console.log(`首页 didShow`)
  }
}

class Index extends Dad {

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Taro.Config = {
    navigationBarTitleText: '首页',
    enablePullDownRefresh: false
  }

  state = {
    list: [1,2,3],
    test: 1111,
    animation: undefined,
    x: 0, y: 0, z: 0
  }

  isExperimental = true

  onPageScroll (scrollTop) {
    console.log(scrollTop)
  }
  onReachBottom () {
    console.log('ReachBottom !')
  }

  componentWillMount () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} willMount`)
    console.log(this.$router)
  }

  componentDidMount () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} didMount`)
    this.setState({
      update: true
    })
  }

  componentWillReceiveProps () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} willReveiveProps`)
  }

  componentDidUpdate () {
  }

  componentWillUnmount () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} willUnmount`)
  }

  componentDidHide () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} didHide`)
  }

  componentWillPreload () {
    console.log(1)
  }

  beforeRouteLeave(from, to, next) {
    console.log(from, to)
    Taro.showModal({
      title: '确定离开吗'
    }).then((res) => {
      if (res.confirm) {
        next(true);
      }

      if (res.cancel) {
        next(false);
      }
    })
  }

  navigateBack = () => {
    Taro.navigateBack({
      delta: 1
    })
  }

  onTest = (args) => {
    console.log(args)
  }

  angle = 1
  animation = Taro.createAnimation({
    transformOrigin: "50% 50%",
    duration: 400,
    timingFunction: "ease",
    delay: 0
  })
  animate = () => {
    this.angle += 180
    this.setState({
      animation: this.animation.rotateZ(this.angle).step().export()
    })
  }
  modal = () => {
    const opts = {
      title: 'title',
      content: 'content',
    }

    if (Date.now() % 2) {
      Taro.showModal({
        ...opts,
        success () {
          console.log('success')
        }
      })
    } else {
      Taro.showModal(opts)
    }
  }
  toast = () => {
    const opts = {
      title: 'title'
    }

    Taro.showToast({
      ...opts
    }).then(() => {
      console.log('promise success')
    })
  }

  actionSheet = () => {
    const opts = {
      itemList: [`test${Date.now().toString().substr(-3, 3)}`]
    }
    if (Date.now() % 2) {
      Taro.showActionSheet({
        ...opts,
        success () {
          console.log('success')
        }
      })
    } else {
      Taro.showActionSheet(opts)
    }
  }

  scrollToTop () {
    Taro.pageScrollTo({ scrollTop: 0 })
  }
  scrollToBottom () {
    Taro.pageScrollTo({ scrollTop: 10000 })
  }

  render () {
    console.log('getApp in page', Taro.getApp())
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} render`)
    let { list, x, y, z } = this.state
    let a = list.map(v => <Text>{v}</Text>)
    return (
      <View className='index'>
        <FuncComp />
        <PlatformSpecificComp />
        <View>
          <Button id="iindex">noop</Button>
        </View>
        <View className='iconfont icon-Eye article_text' />
        <Button onClick={this.animate} hoverClass={'hoverClass'} animation={this.state.animation}>转转转</Button>
        <Button onClick={this.toast}>toast</Button>
        <Button onClick={this.modal}>modal</Button>
        <Button onClick={this.actionSheet}>actionSheet</Button>
        <Text>page 1</Text>
        <Navigator url='pages/issue/4959'>
          navigateTo pages/issue/4959
        </Navigator>
        <Navigator url='/pages/input/input'>
          navigateTo /pages/input/input
        </Navigator>
        <Navigator url='/pages/navigateTest/navigateTest'>
          navigateTo /pages/navigateTest/navigateTest
        </Navigator>
        <Navigator url='/pages/image/image'>
          navigateTo /pages/image/image
        </Navigator>
        <Navigator url='/pages/files/files'>
          navigateTo /pages/files/files
        </Navigator>
        <Navigator url='/pages/modal/modal'>
          navigateTo /pages/modal/modal
        </Navigator>
        <Navigator url='/pages/mobx/mobx'>
          navigateTo /pages/mobx/mobx
        </Navigator>
        <Navigator url='/pages/routers/routers'>
          navigateTo /pages/routers/routers
        </Navigator>
        <Navigator url='/packagea/pages/index/index?type=测试'>
          navigateTo /packagea/pages/index/index?type=测试
        </Navigator>
        <Navigator url='/pages/accelerometer/accelerometer'>
          navigateTo /pages/accelerometer/accelerometer
        </Navigator>
        <Navigator url='/pages/compass/compass'>
          navigateTo /pages/compass/compass
        </Navigator>
        <Navigator url='/pages/location/location'>
          navigateTo /pages/location/location
        </Navigator>
        <Navigator url='/pages/tabbar/tabbar'>
          navigateTo /pages/tabbar/tabbar
        </Navigator>
        <Navigator url='/pages/about/about'>
          navigateTo /pages/about/about
        </Navigator>
        <Navigator url='/pages/actionSheet/actionSheet'>
          navigateTo /pages/actionSheet/actionSheet
        </Navigator>
        <Navigator url='/pages/scrollView/scrollView'>
          navigateTo /pages/scrollView/scrollView
        </Navigator>
        <Navigator url='/pages/scrollView/withoutComponent'>
          navigateTo /pages/scrollView/withoutComponent
        </Navigator>
        <Navigator url='/pages/scrollView/renderWithMap'>
          navigateTo /pages/scrollView/renderWithMap
        </Navigator>
        <Navigator url='/pages/canvas/canvas'>
          navigateTo /pages/canvas/canvas
        </Navigator>
        <Navigator url='/pages/video/video'>
          navigateTo /pages/video/video
        </Navigator>
        <Navigator url='/pages/audio/audio'>
          navigateTo /pages/audio/audio
        </Navigator>
        <Navigator url='https://www.baidu.com'>
          navigateTo https://www.baidu.com
        </Navigator>
        <Navigator openType={'reLaunch'} url='/pages/index/index'>
          reLaunch /pages/index/index
        </Navigator>
        <Navigator openType={'navigateBack'}>
          navigateBack
        </Navigator>
        <Child
          onTest={this.onTest}
          renderHeader={<View>test</View>}
          />
        <ReduxBtn />
        <View><Text>{a}</Text></View>
        <View>
          <Text>
            <pre>{JSON.stringify(this.$router, null, 2)}</pre>
          </Text>
        </View>
        <PureComponent />
        <Image src={img} />
      </View>
    )
  }
}

export {Index}
// export default Index as ComponentClass<PageOwnProps, PageState>
