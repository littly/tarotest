import { Button, View, Text } from '@tarojs/components'
import Taro from '@tarojs/taro'

class Accelerometer extends Taro.Component {
  state = {
  }

  start = () => {
    Taro.startCompass()
  }

  stop = () => {
    Taro.stopCompass()
  }

  componentDidMount () {
    Taro.onCompassChange(res => {
      this.setState({})
    })
  }

  render () {
    const { x, y, z } = this.state
    return (
      <View>
        <Button onClick={this.start}>start</Button>
        <Button onClick={this.stop}>stop</Button>
        <Text>x:{x} </Text>
        <View />
        <Text>y:{y} </Text>
        <View />
        <Text>z:{z}</Text>
      </View>
    )
  }
}

export default Accelerometer
