import Taro from '@tarojs/taro'
import { Button, View, Input, Textarea } from '@tarojs/components'

export default class InputPage extends Taro.Component {
  state = {
    focus: false
  }

  focus = () => {
    this.state.focus = false
    this.setState({
      focus: true
    })
  }

  onChange = e => {
    this.setState({ focus: false })
  }

  render () {
    return (
      <View>
        <Input focus={true} onInput={this.onChange}></Input>
        <Textarea value={'value'} focus={this.state.focus}></Textarea>
        <Button onClick={this.focus}>
          focus
        </Button>
      </View>
    )
  }
}