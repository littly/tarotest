import { View, Navigator, Video } from '@tarojs/components'
import Taro, { createVideoContext as cvc, chooseVideo } from '@tarojs/taro'

import 'taro-ui/dist/style/components/button.scss'

class Btns extends Taro.Component {
  videoContext
  play = () => {
    this.videoContext.play()
  }
  pause = () => {
    this.videoContext.pause()
  }
  seek = () => {
    this.videoContext.seek(3)
  }
  requestFullScreen = () => {
    this.videoContext.requestFullScreen()
  }
  exitFullScreen = () => {
    this.videoContext.exitFullScreen()
  }
  hideStatusBar = () => {
    this.videoContext.hideStatusBar()
  }
  playbackRate = () => {
    this.videoContext.playbackRate()
  }
  sendDanmu = () => {
    this.videoContext.sendDanmu({
      color: '#fff',
      text: String(Date.now())
    })
  }
  showStatusBar = () => {
    this.videoContext.showStatusBar()
  }
  stop = () => {
    this.videoContext.stop()
  }
  chooseVideo = () => {
    chooseVideo({})
      .then(res => {
        console.log(res)
      })
  }
  componentDidMount () {
    this.videoContext = cvc('123')
  }
  render () {
    return (
      <div style={{
        display: 'flex'
      }}>
        <div style={{
          flex: 1
        }}>
          <button onClick={this.chooseVideo}>chooseVideo</button>
          <button onClick={this.play}>play</button>
          <button onClick={this.pause}>pause</button>
          <button onClick={this.seek}>seek</button>
          <button onClick={this.exitFullScreen}>exitFullScreen</button>
          <button onClick={this.hideStatusBar}>hideStatusBar</button>
        </div>
        <div style={{
          flex: 1
        }}>
          <button onClick={this.playbackRate}>playbackRate</button>
          <button onClick={this.requestFullScreen}>requestFullScreen</button>
          <button onClick={this.sendDanmu}>sendDanmu</button>
          <button onClick={this.showStatusBar}>showStatusBar</button>
          <button onClick={this.stop}>stop</button>
        </div>
      </div>
    )
  }
}

class Index extends Taro.Component {
  state = {
    src: 'https://www.runoob.com/try/demo_source/movie.mp4'
  }
  videoRef
  componentDidMount() {
    Taro.startPullDownRefresh({
      success() {console.log('success')},
      complete() {console.log('complete')}
    })
  }
  render() {
    return (
      <View>
        <Navigator openType='navigateBack' delta={1}>back</Navigator>
        <Navigator url={'/pages/index/index'}>to index</Navigator>
        <Btns />
        <Video
          id={"123"}
          src={this.state.src}
          controls={true}
          autoplay={false}
          enableProgressGesture={false}
          objectFit='cover'
          danmuBtn={true}
          showMuteBtn={true}
          initialTime={3}
          ref={ref => {
            this.videoRef = ref;
          }}
          
          danmuList={[
            { text: '第1秒出现的弹幕', color: '#f00', time: 1 },
            { text: '第2秒出现的弹幕', color: '#0f0', time: 2 },
            { text: '第3秒出现的弹幕', color: '#00f', time: 3 },
            { text: '第4秒出现的弹幕', color: '#fff', time: 4 }
          ]}
        />
      </View>
    );
  }
}

export default Index;
