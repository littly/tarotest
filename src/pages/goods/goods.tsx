import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, Image, Navigator } from '@tarojs/components'

// #region 书写注意
// 
// 目前 typescript 版本还无法在装饰器模式下将 Props 注入到 Taro.Component 中的 props 属性
// 需要显示声明 connect 的参数类型并通过 interface 的方式指定 Taro.Component 子类的 props
// 这样才能完成类型检查和 IDE 的自动提示
// 使用函数模式则无此限制
// ref: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/20796
//
// #endregion

type PageStateProps = {
  counter: {
    num: number
  }
}

type PageDispatchProps = {
  add: () => void
  dec: () => void
  asyncAdd: () => any
}

type PageOwnProps = {}

type PageState = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface Index {
  props: IProps;
}

// @connect(({ counter }) => ({
//   counter
// }), (dispatch) => ({
//   add () {
//     dispatch(add())
//   },
//   dec () {
//     dispatch(minus())
//   },
//   asyncAdd () {
//     dispatch(asyncAdd())
//   }
// }))
class Index extends Component {

    /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '商详'
  }

  state = {
    list: [1,2,3],
    test: 1111
  }

  isExperimental = true

  componentWillMount () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} willMount`)
  }

  componentDidMount () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} didMount`)
  }

  componentDidShow () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} didShow`)
  }

  componentWillReceiveProps () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} willReveiveProps`)
  }

  componentWillUnmount () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} willUnmount`)
  }

  componentDidHide () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} didHide`)
  }

  componentWillPreload () {
    console.log(1)
  }

  onTest = (args) => {
    console.log(args)
  }

  render () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} render`)
    let list = this.state.list
    let a = list.map(v => <Text>{v}</Text>)
    return (
      <View className='detail'>
        <Text>page 4</Text>
        <Navigator url={'/pages/about/index?type=测试'} />
        <Navigator openType={'back'} />
        <Image src="//img13.360buyimg.com/babel/s190x210_jfs/t1/14589/26/6202/35481/5c4919e3E40eed767/5a552baa6a2ea066.png!q90!cc_190x210"></Image>
        <Text>这是商品啊</Text>
        <View>
          <Text>
            <pre>{JSON.stringify(this.$router, null, 2)}</pre>
          </Text>
        </View>
        {/* <AtButton>AtButton</AtButton> */}
      </View>
    )
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default Index as ComponentClass<PageOwnProps, PageState>
