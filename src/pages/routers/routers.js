import Taro from '@tarojs/taro'
import { View, Navigator, Button } from '@tarojs/components'

class ImagePage extends Taro.Component {
  appendHash () {
    const location = window.location
    if (location.href.indexOf('#') > -1) return
    else location.href += '#123123'
  }
  render () {
    return (
      <View>
        <Navigator openType='navigateBack' delta={1}>back</Navigator>
        <Button onClick={this.appendHash}>appendHash</Button>
      </View>
    )
  }
}

export default ImagePage
