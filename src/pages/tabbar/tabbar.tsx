import { Button, Text, View, Navigator } from '@tarojs/components'
import Taro, { Component, Config } from '@tarojs/taro'
import { ComponentClass } from 'react'

import 'taro-ui/dist/style/components/button.scss'

type PageStateProps = {
  counter: {
    num: number
  }
}

type PageDispatchProps = {
  add: () => void
  dec: () => void
  asyncAdd: () => any
}

type PageOwnProps = {}

type PageState = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface Tabbar {
  props: IProps;
}

class Tabbar extends Component {
  config: Config = {
    navigationBarTitleText: 'tabbar测试页'
  }

  state = {
    list: [1,2,3],
    test: 1111
  }

  isExperimental = true

  componentWillMount () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} willMount`)
  }

  componentDidMount () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} didMount`)
  }

  componentDidShow () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} didShow`)
  }

  componentWillReceiveProps () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} willReveiveProps`)
  }

  componentWillUnmount () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} willUnmount`)
  }

  componentDidHide () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} didHide`)
  }

  componentWillPreload () {
    console.log(1)
  }

  onTest = (args) => {
    console.log(args)
  }

  switchTabRel = () => {
    Taro.switchTab({
      url: '../about/about'
    }).then(res => {
      console.log(res)
    })
  }

  switchTabAbs = () => {
    Taro.switchTab({
      url: '/pages/index/index?key=value'
    }).then(res => {
      console.log(res)
    })
  }

  switchTabErr = () => {
    Taro.switchTab({
      url: '/pages/detail/index'
    }).then(res => {
      console.log(res)
    })
  }

  setTabBarBadge = () => {
    Taro.setTabBarBadge({
      index: 0,
      text: 'text'
    })
  }

  removeTabBarBadge = () => {
    Taro.removeTabBarBadge({
      index: 0
    })
  }

  showTabBarRedDot = () => {
    Taro.showTabBarRedDot({
      index: 1
    })
  }

  hideTabBarRedDot = () => {
    Taro.hideTabBarRedDot({
      index: 1
    })
  }

  showTabBar = () => {
    Taro.showTabBar()
  }

  hideTabBar = () => {
    Taro.hideTabBar()
  }

  showTabBarWithAnimation = () => {
    Taro.showTabBar({
      animation: true
    })
  }

  hideTabBarWithAnimation = () => {
    Taro.hideTabBar({
      animation: true
    })
  }

  render () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} render`)
    let list = this.state.list
    let a = list.map(v => <Text>{v}</Text>)
    return (
      <View className='index'>
        <Navigator openType="back" />
        <Button onClick={this.switchTabAbs}>switchTabAbs</Button>
        <Button onClick={this.setTabBarBadge}>setTabBarBadge</Button>
        <Button onClick={this.removeTabBarBadge}>removeTabBarBadge</Button>
        <Button onClick={this.showTabBarRedDot}>showTabBarRedDot</Button>
        <Button onClick={this.hideTabBarRedDot}>HideTabBarRedDot</Button>
        <Button onClick={this.showTabBar}>showTabBar</Button>
        <Button onClick={this.hideTabBar}>HideTabBar</Button>
        <Button onClick={this.showTabBarWithAnimation}>showTabBarWithAnimation</Button>
        <Button onClick={this.hideTabBarWithAnimation}>HideTabBarWithAnimation</Button>
        <View>
          <Text>
            <pre>{JSON.stringify(this.$router, null, 2)}</pre>
          </Text>
        </View>
      </View>
    )
  }
}

export default Tabbar as ComponentClass<PageOwnProps, PageState>
