import { Component } from '@tarojs/taro'
import { inject, observer } from '@tarojs/mobx'

@inject('indexStore')
@observer
export default class Mobx extends Component {
  customProp = 'test'
  componentDidShow () {
    debugger
    console.log(this)
  }
  render () {
    return <div />
  }
}
