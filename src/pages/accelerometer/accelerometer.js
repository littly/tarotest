import { Button, View, Text } from '@tarojs/components'
import Taro from '@tarojs/taro'

class Accelerometer extends Taro.Component {
  state = {
    x: 0,
    y: 0,
    z: 0
  }

  listenGame = () => {
    Taro.startAccelerometer({
      interval: 'game'
    })
  }

  listenUi = () => {
    Taro.startAccelerometer({
      interval: 'ui'
    })
  }

  listenNormal = () => {
    Taro.startAccelerometer({
      interval: 'normal'
    })
  }

  stop = () => {
    Taro.stopAccelerometer()
  }

  componentDidMount () {
    Taro.onAccelerometerChange(res => {
      this.setState({
        x: res.x,
        y: res.y,
        z: res.z
      })
    })
  }

  render () {
    const { x, y, z } = this.state
    return (
      <View>
        <Button onClick={this.listenGame}>gameMode</Button>
        <Button onClick={this.listenUi}>uiMode</Button>
        <Button onClick={this.listenNormal}>normalMode</Button>
        <Button onClick={this.stop}>stop</Button>
        <Text>x:{x} </Text>
        <View />
        <Text>y:{y} </Text>
        <View />
        <Text>z:{z}</Text>
      </View>
    )
  }
}

export default Accelerometer
