import { View, Button } from '@tarojs/components'
import Taro from '@tarojs/taro'

class Navigation extends Taro.Component {
  componentWillReceiveProps () {}
  componentDidHide() {
    throw new Error()
  }
  render () {
    return (
      <Button onClick={() => {
        Taro.navigateTo({
          url: '/pages/index/index'
        })
      }}>
        navigate
      </Button>
    )
  }
}

export default Navigation
