import { ComponentClass } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View, Button, Text, Navigator } from '@tarojs/components'
import { connect } from '@tarojs/redux'
import { add, minus, asyncAdd } from '../../actions/counter'

import './about.css'

// #region 书写注意
// 
// 目前 typescript 版本还无法在装饰器模式下将 Props 注入到 Taro.Component 中的 props 属性
// 需要显示声明 connect 的参数类型并通过 interface 的方式指定 Taro.Component 子类的 props
// 这样才能完成类型检查和 IDE 的自动提示
// 使用函数模式则无此限制
// ref: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/20796
//
// #endregion

type PageStateProps = {
  counter: {
    num: number
  }
}

type PageDispatchProps = {
  add: () => void
  dec: () => void
  asyncAdd: () => any
}

type PageOwnProps = {}

type PageState = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface About {
  props: IProps;
}

@connect(({ counter }) => ({
  counter
}), (dispatch) => ({
  add () {
    dispatch(add())
  },
  dec () {
    dispatch(minus())
  },
  asyncAdd () {
    dispatch(asyncAdd())
  }
}))
class About extends Component {

    /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '关于我们'
  }
  isExperimental = true

  onPullDownRefresh () {

  }

  componentWillMount () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} willMount`)
  }

  componentDidMount () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} didMount`)
  }

  componentDidShow () {
    console.log(this)
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} didShow`)
  }

  componentWillReceiveProps () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} willReveiveProps`)
  }

  componentWillUnmount () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} willUnmount`)
  }

  componentDidHide () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} didHide`)
  }

  navigate () {
    Taro.navigateTo({
      url: '/packagea/pages/index/index?type=测测试'
    })
  }

  navigateBack = () => {
    Taro.navigateBack({
      delta: 1
    })
  }

  relaunch = () => {
    Taro.reLaunch({
      url: '/pages/index/index'
    })
  }

  navigateIndex = () => {
    Taro.navigateTo({
      url: '/pages/index/index'
    })
  }

  render () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} render`)
    return (
      <View className='about'>
        <Text>page 2</Text>
        <Button onClick={this.navigate}>navigate</Button>
        <Button onClick={this.navigateBack}>back</Button>
        <Button onClick={this.relaunch}>relaunch</Button>

        <Button onClick={this.navigateIndex}>
          navigateTo /pages/index/index
        </Button>
        
        <Button className='add_btn' onClick={this.props.add}>+</Button>
        <Button className='dec_btn' onClick={this.props.dec}>-</Button>
        <Button className='dec_btn' onClick={this.props.asyncAdd}>async</Button>
        <View><Text>{this.props.counter.num}</Text></View>
        <View><Text>Hello, about</Text></View>
        <View>
          <Text>
            <pre>{JSON.stringify(this.$router, null, 2)}</pre>
          </Text>
        </View>  
      </View>
    )
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default About as ComponentClass<PageOwnProps, PageState>
