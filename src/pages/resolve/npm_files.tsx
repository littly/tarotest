import { View } from '@tarojs/components'
import Taro from '@tarojs/taro'
import { ComponentClass } from 'react'

type PageOwnProps = {}

type PageState = {}

class Index extends Taro.Component {
  config: Taro.Config = {
    navigationBarTitleText: '首页',
    enablePullDownRefresh: false
  }

  render () {
    return (
      <View className='index' />
    )
  }
}

export default Index as ComponentClass<PageOwnProps, PageState>
