import 'taro-ui/dist/style/components/button.scss';

import { Button, Text, View } from '@tarojs/components';
import Taro, { Component, Config } from '@tarojs/taro';
import { ComponentClass } from 'react';

import PureComponent from '../../components/pure_component';

type PageStateProps = {
  counter: {
    num: number
  }
}

type PageDispatchProps = {
  add: () => void
  dec: () => void
  asyncAdd: () => any
}

type PageOwnProps = {}

type PageState = {}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps

interface Tabbar {
  props: IProps;
}

class Tabbar extends Component {
  config: Config = {
    navigationBarTitleText: 'tabbar测试页'
  }

  state = {
    list: [1,2,3],
    test: 1111
  }

  isExperimental = true

  componentWillMount () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} willMount`)
  }

  componentDidMount () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} didMount`)
  }

  componentDidShow () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} didShow`)
  }

  componentWillReceiveProps () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} willReveiveProps`)
  }

  componentWillUnmount () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} willUnmount`)
  }

  componentDidHide () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} didHide`)
  }

  componentWillPreload () {
    console.log(1)
  }

  navigateBack = () => {
    Taro.navigateBack({
      delta: 1
    })
  }

  showActionSheet = () => {
    Taro.showActionSheet({
      itemList: ['a', 'b', 'c'],
      success: res => {
        console.log(res)
      }
    })
  }

  render () {
    this.isExperimental && console.log(`${this.config.navigationBarTitleText} render`)
    let list = this.state.list
    let a = list.map(v => <Text>{v}</Text>)
    return (
      <View className='index'>
        <Button onClick={this.navigateBack}>back</Button>
        <Button onClick={this.showActionSheet}>showActionSheet</Button>
      </View>
    )
  }
}

export default Tabbar as ComponentClass<PageOwnProps, PageState>
