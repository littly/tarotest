import { View, Navigator } from '@tarojs/components'
import Taro from '@tarojs/taro'
import 'taro-ui/dist/style/components/button.scss'

class Index extends Taro.Component {
  openLocation () {
    Taro.openLocation({
      latitude: 22.55329,
      longitude: 113.88308
    }).then(res => {
      console.log('success', res)
    }).catch(res => {
      console.log('fail', res)
    })
  }
  getLocation () {
    Taro.getLocation({
      type: 'wgs84'
    }).then(res => {
      console.log('success', res)
    }).catch(res => {
      console.log('fail', res)
    })
  }
  chooseLocation () {
    Taro.chooseLocation()
      .then(res => {
        console.log(res)
      })
      .catch(res => {
        console.log(res)
      })
  }
  componentDidMount() {
  }
  render() {
    return (
      <View>
        <Navigator openType='navigateBack' delta={1}>back</Navigator>
        <Navigator url={'/pages/index/index'}>to index</Navigator>
        <button type={'primary'} onClick={this.openLocation}>openLocation</button>
        <button type={'primary'} onClick={this.getLocation}>getLocation</button>
        <button type={'primary'} onClick={this.chooseLocation}>chooseLocation</button>
      </View>
    );
  }
}

export default Index;
