import Taro, {Component} from '@tarojs/taro'
import { Image, View, Navigator, Button } from '@tarojs/components'

class ImagePage extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  getImageInfo = () => {
    Taro.getImageInfo({
      src: 'http://www.baidu.com/bd_logos1.png'
    }).then(res => {
      console.log(res)
    }).catch(err => {
      console.log(err)
    })
  }
  previewImage = () => {
    Taro.previewImage({
      urls: [
        'https://m.360buyimg.com/img/jfs/t1/16074/5/8686/211783/5c77fbacE50bc5fe1/681ba09abcf55c11.png',
        'https://m.360buyimg.com/img/jfs/t1/22354/16/8646/21917/5c77fbacEdee5c349/e6af50961d434f0.png',
        'https://m.360buyimg.com/img/jfs/t1/14453/5/8776/198331/5c77fbacE24aec263/74ceb998fe9f5059.png',
        'https://m.360buyimg.com/img/jfs/t1/21781/18/8661/46118/5c77fbacE28c35efd/ba6ed6939fa041ba.png',
        'https://m.360buyimg.com/img/jfs/t1/29124/32/8757/41440/5c77fbacEa1b2206c/fba8b10d73136327.png',
        'https://m.360buyimg.com/img/jfs/t1/16633/12/8583/53807/5c77fbacE239aa77a/0d515de24217f5b5.png',
        'https://m.360buyimg.com/img/jfs/t1/14351/23/8588/28303/5c77fbacEf0ec647b/0b8652e5a5bb49a7.png',
        'https://m.360buyimg.com/img/jfs/t1/11020/15/9616/262595/5c77fbadE6f554c3f/c4d4bc42d65508cd.png',
        'https://m.360buyimg.com/img/jfs/t1/14100/24/8582/236551/5c77fbb1Ef1ec35cc/751d4324349728b8.png'
      ],
      // current: 'https://www.baidu.com/bd_logo1.png',
      current: '//m.360buyimg.com/img/jfs/t1/16633/12/8583/53807/5c77fbacE239aa77a/0d515de24217f5b5.png'
    })
  }
  chooseImage = () => {
    Taro.chooseImage({
      count: 1
    }).then(res => {
      console.log(res)
      Taro.uploadFile({
        url: 'https://httpbin.org/post',
        filePath: res.tempFiles[0].path,
        name: 'file'
      })
    })
  }
  componentDidMount () {
    console.log(this.$router)
  }
  componentDidShow() {}
  render () {
    return (
      <View>
        <Navigator openType='navigateBack' delta={1}>back</Navigator>
        <Button onClick={this.getImageInfo}>获取图片信息</Button>
        <Button onClick={this.previewImage}>预览图片</Button>
        <Button onClick={this.chooseImage}>选择图片</Button>
        <Image
          src='http://www.baidu.com/bd_logo1.png'
          lazyload
          imgProps={{
            crossOrigin: 'anonymous'
          }}
          onLoad={() => {
            console.log('onload')
          }} />
      </View>
    )
  }
}

export default ImagePage
