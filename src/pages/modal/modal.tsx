import Taro, { Component } from "@tarojs/taro";

export default class Modal extends Component {
  componentDidMount() {
    Taro.showToast({
      title: "abc"
    }).then(() => {
      console.log('abc')
    })
    setTimeout(() => {
      Taro.showLoading(); //显示abc
    }, 3000);
    Taro.showModal({
      title: "提示",
      content: "content"
    });
    Taro.showModal({
      title: "title",
      content: "content2"
    })
  }
  render() {
    return (
      <div>
        Elit excepteur irure adipisicing laboris culpa dolore dolor. Magna
        cillum ea ullamco duis id sunt. Veniam ullamco cillum ullamco proident
        cillum aliquip labore. Ut id dolor velit cillum. Id qui deserunt nulla
        aliqua culpa enim aliqua velit incididunt. Dolor velit officia ad
        eiusmod irure ex aliquip minim ullamco reprehenderit in. Minim ipsum
        veniam reprehenderit magna veniam aute est do ea elit non duis et.
        Excepteur laborum sunt ullamco ipsum magna nisi velit aliqua
        reprehenderit. Eu sint dolore labore tempor fugiat laborum reprehenderit
        ad. Exercitation nostrud aliqua ullamco laboris aliquip quis ullamco
        enim voluptate mollit fugiat ex enim. Ipsum occaecat officia nisi Lorem
        officia enim mollit mollit ut non minim. Ullamco amet esse do nulla
        adipisicing. Excepteur irure et elit irure. Aute sit commodo dolore
        fugiat velit Lorem Lorem. Excepteur deserunt anim fugiat nostrud eiusmod
        eiusmod aliqua. Laboris dolore dolore laboris commodo cupidatat sunt
        pariatur commodo tempor culpa aliquip. Qui dolor irure aliqua fugiat
        pariatur ad reprehenderit occaecat Lorem consequat. Excepteur
        adipisicing in exercitation qui minim tempor. Deserunt quis anim amet
        pariatur dolor. Elit aliqua quis ullamco pariatur proident cillum
        nostrud sunt consequat do aute aliquip anim nisi. Qui officia labore
        esse deserunt irure labore aliqua do aliqua incididunt qui veniam
        consectetur. Officia culpa aliquip aliqua velit culpa veniam nostrud
        consectetur culpa anim ipsum sit amet pariatur. Labore tempor consequat
        deserunt adipisicing id Lorem ea Lorem dolore dolor ea Lorem mollit.
        Consequat cillum aute laboris ex nisi pariatur non mollit culpa quis
        quis deserunt id eiusmod. Aute labore sunt nisi anim qui labore velit
        eiusmod esse sint ipsum nisi nostrud reprehenderit. Do Lorem ullamco
        consequat voluptate. Ad duis ullamco sunt minim cupidatat fugiat mollit
        ipsum et voluptate laborum velit Lorem. Commodo nulla officia pariatur
        dolore dolor consequat enim Lorem. Ea ex nostrud pariatur culpa commodo.
        Officia proident culpa nisi cupidatat occaecat reprehenderit nisi elit
        magna velit non ut eiusmod sunt. Ullamco cillum culpa nisi ipsum labore
        nisi reprehenderit qui amet sit fugiat pariatur non tempor. Fugiat dolor
        eu eiusmod proident culpa duis occaecat aute amet. Esse irure pariatur
        aute velit. Sit et proident sit pariatur est cillum nisi. Reprehenderit
        tempor exercitation exercitation ea aliqua cupidatat exercitation eu
        voluptate. Ea consectetur qui excepteur aliquip aute et. Sunt commodo
        velit exercitation aute sit deserunt ea veniam incididunt incididunt
        cillum quis ex. Non fugiat enim elit aute ipsum. Sint officia officia ut
        dolor aliquip in cupidatat minim veniam magna pariatur sint Lorem magna.
        Labore labore amet laborum ex laboris anim. Ad sunt voluptate voluptate
        nostrud deserunt esse in nostrud amet nisi commodo veniam quis.
        Exercitation culpa ex pariatur est qui. Sit ullamco eu veniam irure ad
        anim dolore exercitation est Lorem. Est laborum minim quis est tempor
        enim reprehenderit anim minim. Fugiat et consectetur non aliqua
        incididunt amet non consequat. Culpa nisi laboris aliquip aliqua
        adipisicing sit. Voluptate non minim sit nisi aliqua reprehenderit Lorem
        in consequat ea do. Elit adipisicing magna ex in anim dolore enim anim
        consectetur aute. Veniam qui magna do irure officia. Qui amet nisi anim
        id laborum veniam elit cillum velit in ad minim aliquip est.
      </div>
    );
  }
}
