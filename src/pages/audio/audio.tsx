import { View, Navigator, Audio } from '@tarojs/components'
import Taro from '@tarojs/taro'
import 'taro-ui/dist/style/components/button.scss'

class Index extends Taro.Component {
  audioContext
  play = () => {
    this.audioContext.play()
  }
  pause = () => {
    this.audioContext.pause()
  }
  seek = () => {
    this.audioContext.seek(3)
  }
  stop = () => {
    this.audioContext.stop()
  }
  destroy = () => {
    this.audioContext.destroy()
  }
  log = () => {
    console.log(this.audioContext)
  }
  componentDidMount() {
    this.audioContext = Taro.createInnerAudioContext()
    this.audioContext.src = 'http://ws.stream.qqmusic.qq.com/M500001VfvsJ21xFqb.mp3?guid=ffffffff82def4af4b12b3cd9337d5e7&uin=346897220&vkey=6292F51E1E384E06DCBDC9AB7C49FD713D632D313AC4858BACB8DDD29067D3C601481D36E62053BF8DFEAF74C0A5CCFADD6471160CAF3E6A&fromtag=46'
    this.audioContext.onPlay(() => console.log('play'))
    this.audioContext.onPause(() => console.log('pause'))
    this.audioContext.onStop(() => console.log('stop'))
  }
  render() {
    return (
      <View>
        <Navigator openType='navigateBack' delta={1}>back</Navigator>
        <Navigator url={'/pages/index/index'}>to index</Navigator>
        {/* <Audio
          id={'audio'}
          src={'http://ws.stream.qqmusic.qq.com/M500001VfvsJ21xFqb.mp3?guid=ffffffff82def4af4b12b3cd9337d5e7&uin=346897220&vkey=6292F51E1E384E06DCBDC9AB7C49FD713D632D313AC4858BACB8DDD29067D3C601481D36E62053BF8DFEAF74C0A5CCFADD6471160CAF3E6A&fromtag=46'}
          loop={false}
          controls={false}
          poster={'http://y.gtimg.cn/music/photo_new/T002R300x300M000003rsKF44GyaSk.jpg?max_age=2592000'}
          author={'author'}
          name={'name'}
        /> */}
        <div style={{
          display: 'flex'
        }}>
          <div style={{
            flex: 1
          }}>
            <button onClick={this.play}>play</button>
            <button onClick={this.pause}>pause</button>
            <button onClick={this.seek}>seek</button>
          </div>
          <div style={{
            flex: 1
          }}>
            <button onClick={this.destroy}>destroy</button>
            <button onClick={this.stop}>stop</button>
            <button onClick={this.log}>log</button>
          </div>
        </div>
      </View>
    );
  }
}

export default Index;
