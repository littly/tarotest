import Taro, { useEffect, useState } from '@tarojs/taro'
import { View, Swiper, SwiperItem } from '@tarojs/components'

const Index = () => {
  const [current, setCurrent] = useState(0)
  const [text, setText] = useState('')

  console.log('text1', text)

  useEffect(() => {
    setText('rendered')
  }, [])

  const handleChange = (e) => {
    console.log('text2', text)
    setCurrent(e.detail.current)
  }

  return (
    <View>
      <Swiper
        indicatorDots
        current={current}
        autoplay={true}
        interval={500}
        circular={true}
        onChange={handleChange}
      >
        <SwiperItem>
          <View>1asfasdfasdfadfadf</View>
        </SwiperItem>
        <SwiperItem>
          <View>2asdfasdfasdfadfadsf</View>
        </SwiperItem>
        <SwiperItem>
          <View>3asdfsdasfadfadfasdf</View>
        </SwiperItem>
      </Swiper>
    </View>
  )
}

export default Index