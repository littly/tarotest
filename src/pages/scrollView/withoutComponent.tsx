import Taro from '@tarojs/taro'

export default class PullWithoutComponent extends Taro.Component {
  onPullDownRefresh () {}

  render () {
    return (
      <div style={{
        height: '300px',
        background: '#bbb'
      }} />
    )
  }
}