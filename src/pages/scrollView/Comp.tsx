import { Button } from '@tarojs/components';
import Taro from '@tarojs/taro';

export class Comp extends Taro.Component {
  render() {
    return (
      <Button onClick={() => {
        Taro.startPullDownRefresh();
      }}>Button</Button>
    )
  }
}
