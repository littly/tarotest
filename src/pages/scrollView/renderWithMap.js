import Taro, { Component } from "@tarojs/taro";
import { View, Text, ScrollView, Button } from "@tarojs/components";
import "./index.scss";
import { AtList, AtListItem, AtActivityIndicator } from "taro-ui";

export default class Index extends Component {
  constructor() {
    super(...arguments);
    this.state = {
      arrData: [{
        title: 'title',
        note: 'note',
        arrow: 'arrow',
        thumb: 'thumb'
      }, {
        title: 'title',
        note: 'note',
        arrow: 'arrow',
        thumb: 'thumb'
      }, {
        title: 'title',
        note: 'note',
        arrow: 'arrow',
        thumb: 'thumb'
      }, {
        title: 'title',
        note: 'note',
        arrow: 'arrow',
        thumb: 'thumb'
      }, {
        title: 'title',
        note: 'note',
        arrow: 'arrow',
        thumb: 'thumb'
      }, {
        title: 'title',
        note: 'note',
        arrow: 'arrow',
        thumb: 'thumb'
      }, {
        title: 'title',
        note: 'note',
        arrow: 'arrow',
        thumb: 'thumb'
      }],
      isLoading: false,
      noMore: false,
      noMoreLoading: false
    };
  }

  config = {
    navigationBarTitleText: "首页",
    enablePullDownRefresh: true,
    onReachBottomDistance: 50
  };

  componentWillMount() {}

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidShow() {}

  componentDidHide() {}

  handleClick = value => {
    this.setState({
      current: value
    });

    switch (value) {
      case 0:
        Taro.redirectTo({
          url: "pages/plan/plan"
        });
        break;

      case 1:
        Taro.redirectTo({
          url: "pages/user/user"
        });
        break;
    }
  };

  onScrollToUpper = () => {
    Taro.startPullDownRefresh()
      .then(() => {
        console.log('成功')
      })
    console.log("滚动到顶部");
  };

  onScrollToLower = () => {
    console.log("滚动到底部");
    setTimeout(() => {
      this.setState({
        noMore: true
      });
    }, 2000);
  };

  //上拉事件
  onReachBottom() {
    console.log("上拉");
  }

  //下拉事件
  onPullDownRefresh() {
    console.log("下拉");
  }

  render() {
    let { arrData, isLoading, noMore, noMoreLoading } = this.state;
    return (
      <ScrollView
        className="scroll-style"
        scrollY
        // scrollWithAnimation
        enableBackToTop
        scrollTop="0"
        lowerThreshold="0"
        upperThreshold="20"
        onScrollToUpper={this.onScrollToUpper}
        onScrollToLower={this.onScrollToLower}
      >
        <AtList>
          {arrData.map((item, index) => {
            let { id, title, arrow, thumb, note } = item;
            return (
              <AtListItem
                title={title}
                note={note}
                arrow={arrow}
                thumb={thumb}
              />
            );
          })}
        </AtList>
        {isLoading ? null : noMore ? (
          <View className="at-row at-row__justify--center">
            <Text className="no-more">没有更多数据了</Text>{" "}
          </View>
        ) : (
          <AtActivityIndicator
            className="indicator"
            size={32}
            content="加载中..."
          />
        )}
      </ScrollView>
    );
  }
}
