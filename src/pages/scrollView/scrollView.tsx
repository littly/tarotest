import { ScrollView, Text, View, Navigator, Button } from '@tarojs/components'
import Taro, { pageScrollTo } from '@tarojs/taro'
import { Comp } from './Comp';

class Index extends Taro.Component {
  componentDidMount() {
    Taro.startPullDownRefresh({
      success() {console.log('success')},
      complete() {console.log('complete')}
    })
  }
  componentDidShow() {
  }
  onPullDownRefresh () {
    console.log('pulldownrefresh')
    // setTimeout(() => {
    //   Taro.stopPullDownRefresh()
    // }, 10)
  }
  startPullDownRefresh () {
    Taro.startPullDownRefresh().then(() => {
      console.log('start')
    })
  }
  stopPullDownRefresh () {
    Taro.stopPullDownRefresh()
  }
  onReachBottom () {
    console.log('reachBottom')
  }
  onPageScroll (res) {
    console.log(res)
  }
  scrollToTop () {
    Taro.pageScrollTo({ scrollTop: 0 })
  }
  scrollToBottom () {
    Taro.pageScrollTo({ scrollTop: 10000 })
  }
  render() {
    return (
      <View>
        <Navigator openType='navigateBack' delta={1}>back</Navigator>
        <Navigator url={'/pages/index/index'}>to index</Navigator>
        <Button onClick={this.scrollToBottom}>bottom</Button>
        <Comp></Comp>
        <ScrollView
          onScrollToUpper={() => {
            Taro.startPullDownRefresh()
            // console.log('upper')
          }}
          scrollY={true}
          style={"height: 200px"}>
          <View>
            <Text>
              不第步：改事電嚴山今導形；出規大以業個不；的回產心受，民查作明會保主有事以。健把再沒歌馬入著有學身：面人都！如認百主，長不苦議中了世至級手像物來造環廣的年就：體叫的！年從亮。乎當石賽定自。公如史。便為量組那生里有的香未議過下營員人程起，天來朋落地史黑黃吃認論代水。

              留家報古不度教有影產驗狀夜竟接口來必。最性人負親生都先狀來城如……價雲力的天弟紀文規下聞步說不，來大魚的病去工大了文作從滿上文們市，轉的界身創了青保我的。

              動率三！長人行了苦樂片，能上人金以一致國空件接是持子來試開中美何但節。樂快筆。過商遠評說準麼答為到的了提此易教的情麼重人快市期卻生寫理戰賽兒主給主不……比故看。世強好次！什之地。那取詩省嗎演心影然維：不要這以場，正職人美顧為統子但苦；送陽化想濟的點、約省子導出且是傷並點差靈亮，活消的我朋出，致知喜權何才報以標該辦們消可跑配水人邊可車到三強民開很一我看當一春術快不來更風地美關不事知石乎候有施水因同中不作無意研這；功打兩何想，手已的立年目？親己山經險不主會打究多發應，習國不。人答美加藥充意媽打雖真傷，到們入林燈學系。河能而來商其展意言活去前裡，把地英由持好代境連考會女明友備！眼歷中全覺自子字男部你；樂沒會兒星，我選使那了了任天。子盡開給的真生油司女北室機。

              品不正條見的團險有業目？聞灣社子出大急步看福作要……工去數愛要分球帶檢卻成可電體片備美。
            </Text>
          </View>
        </ScrollView>
        <Button onClick={this.startPullDownRefresh}>startPullDownRefresh</Button>
        <Button onClick={this.stopPullDownRefresh}>stopPullDownRefresh</Button>
        <View>
          <Text>心未地土喜汽國我用出友不心心品議時學臉著有個三到燈：百氣日主非致腳字新顯紀要才門，多中北白出！拿的他山代吃中；那舉會大呢，見及上天和手時有身了必服出她到覺真種流圖要據一士應是，人洋反須小……達廣語往有成急研光為工有並以有眼部政把因各傳為主告，主那員行成質但晚我部具去難建又不的那且外慢家它酒復自容性愛了或我能地現是童家；打切人地自之校可許想後善亮，到的條。了識自善小同傳什的上個。</Text>
        </View>
        <View>
          <Text>回白大們。面麼二藝提技受它之性去利兒能臺我易著是流的話臺然以的影身還小綠……總的童子費的期過化士作食能分須上活童，趣面合到問命分，不我我之滿整；制統上及公生張，我平長非場的；座企的平是位此？</Text>
        </View>
        <View>
          <Text>也身現本變本試線數學業飛來地，例館長大到，包且洲心會北交參，們先們動，兒麗兩中環共東是、費一因書同鄉：民現色的間道正這於主親出，世要人？</Text>
        </View>
        <View>
          <Text>界到工難，興神見！</Text>
        </View>
        <View>
          <Text>評的看此事陸有中。運底去開許仍來密。</Text>
        </View>
        <View>
          <Text>問安物面。前注料，此老無以他意玩，留得如？們慢發相雜以注有大怎西的說色變時己藝都一師是簡孩心愛場，紅老風近古著作何間麼作學費確：從運爭的安客得們。</Text>
        </View>
        <View>
          <Text>度樹例花說？得謝整成裡她親同了中進了感、衣下正！庭片看民會間；接現戰海口人現為超真物常同快是成全動人長是又心生值重葉明一案車需沒人名為入男龍。們只物其死！</Text>
        </View>
        <View>
          <Text>國家樣女……性為北從。分師麼牛色大酒了緊李有回，天去小術我趣比我驚了老不如化一了傳校，提一學氣，用作來里設知對提馬，得大感今會只子古動起史的後機印公，相應操進首設不物，地食後體部發出球命不；票高民懷重。外只已亮如長改費率形：教因就可已，的是得的許信戰議再的會生嗎媽細於辦基長的那國相可品顧，器通成做為的始大開。家研麼智邊太花主本心快的，朋英送接作正進國究民教是星的電確山？員命應下高五品有了的。事他面像……則點下，事樹告些子文她？突決它看學，女的生臺界見間命，登就情的花外了而成地，知的治溫心發係那的分。</Text>
        </View>
        <View>
          <Text>資自望，情步信有風他看春教電病回樣雖起種爭工張樣總。英機何統仍投！就讓病先門事道影便未資，機始面，原有義發能下，了李點，最的吃最子史。格母有。見天經二表營！政天眼自坐最建費臉基病，正用海是斷，怎化到最越？身難顧活？爸文師體機雙直通現技我球的。</Text>
        </View>
        <View>
          <Text>於一外？</Text>
        </View>
        <View>
          <Text>示邊才平黨推前大樂自沒相打。邊裡不……縣她們前同化是如得外顧成的山調三畫其山排。道前公密怎型做一，有他請，要全不後設正口，為道與品去海銀手他決洲者，冷感眾……來要如為說他向想城一育說，直面國況規開，海人實教微也深廣會黨有回！壓生廣畫法的人。家變命：對此性明完顧園有少。</Text>
        </View>
        <View>
          <Text>上保為開年所，外出縣必就民然年叫治生可注們再育體。</Text>
        </View>
        <Button onClick={this.scrollToTop}>top</Button>
      </View>
    );
  }
}

export default Index;
