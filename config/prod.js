const path = require('path')
const uglifyPlugin = require('uglifyjs-webpack-plugin')
const optimize = require('optimize-css-assets-webpack-plugin')

module.exports = {
  env: {
    NODE_ENV: '"production"'
  },
  defineConstants: {
  },
  weapp: {},
  h5: {
    publicPath: '/h5',
    webpackChain (chain) {
      chain.plugin('analyzer')
        .use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin, [])
      // chain.merge({
      //   optimization: {
      //     minimizer: [
      //       new uglifyPlugin({}),
      //       new optimize({})
      //     ]
      //   }
      // })
    },
  }
}



// chain.plugin('analyzer')
      //   .use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin, [])