const path = require('path')
const util = require('util')

const config = {
  alias: {
    '@tarojs/components': '/Users/xiexiaoli/Project/taro/packages/taro-components',
    // '@tarojs/redux-h5': '/Users/xiexiaoli/Project/taro/packages/taro-redux-h5',
    '@tarojs/router': '/Users/xiexiaoli/Project/taro/packages/taro-router',
    // '@tarojs/taro': '/Users/xiexiaoli/Project/taro/packages/taro',
    // '@tarojs/taro-h5': '/Users/xiexiaoli/Project/taro/packages/taro-h5',
    // '@tarojs/mobx-h5': '/Users/xiexiaoli/Project/taro/packages/taro-mobx-h5',
    '@/src': path.resolve(__dirname, '..', 'src'),
    '@components': path.resolve(__dirname, '..', 'src/components')
    // 'taro-ui': '/Users/xiexiaoli/Project/taro-ui'
  },
  h5: {
    transformOnly: false,
    devServer: {
      host: '0.0.0.0',
      open: true
    },
    esnextModules: ['taro-ui'],
    module: {
      postcss: {
        autoprefixer: {
          enable: true
        },
        cssModules: {
          config: {
            generateScopedName: '[name]__[local]__[hash:base64:5]'
          },
          enable: false
        }
      }
    },
    output: {
      chunkFilename: 'js/[name].[chunkhash:8].js',
      filename: 'js/[name].[hash:8].js'
    },
    publicPath: '/h5/test',
    router: {
      basename: '/h5/',
      customRoutes: {
        '/pages/navigateTest/navigateTest': '/navigateTest',
        '/pages/index/index': '/'
      },
      renamePagename(pagename) {
        return pagename.replace(/pages/, '').replace(/\//g, '~')
      },
      // mode: 'multi',
      // lazyload: true
    },
    staticDirectory: 'static',
    webpackChain(chain) {
      chain.module.rule('jsx')
        .use('babelLoader')
          .tap(args => ({
            ...args,
            babelrc: false
          }))
    },
  },
  // copy: {
  //   patterns: [{
  //     from: '.eslintrc',
  //     to: `dist/${process.env.TARO_ENV}`
  //   }]
  // },
  defineConstants: {
    "LOCATION_APIKEY": JSON.stringify('LCDBZ-EXA6X-NXQ4T-ZJLJ5-6MBFJ-PJBMO')
  },
  designWidth: 750 * 750 / 640,
  outputRoot: `dist/${process.env.TARO_ENV}`,
  plugins: {
    uglify: {
      enable: true
    },
    babel: {
      plugins: [
        require.resolve('babel-plugin-transform-decorators-legacy'),
        require.resolve('babel-plugin-transform-class-properties'),
        require.resolve('babel-plugin-transform-object-rest-spread'),
        require.resolve('babel-plugin-preval')
        // [require.resolve('babel-plugin-transform-runtime'), {
        //   helpers: false,
        //   polyfill: false,
        //   regenerator: true
        // }]
      ],
      presets: [[require.resolve('babel-preset-env'), {
        modules: false
      }]]
    },
    sass: {
      data: '$nav-height: 48px;'
    }
  },
  projectName: 'taroTest',
  sourceRoot: 'src',
  weapp: {
    compile: {
      include: ['@djtaro/common'],
      compressTemplate: false
    }
  }
}

module.exports = function (merge) {
  if (process.env.NODE_ENV === 'development') {
    return merge({}, config, require('./dev'))
  }
  return merge({}, config, require('./prod'))
}
