const config = {
  defineConstants: {
    
      APMTAROID: 'taro_fbd319b0-6290-11ea-9dcc-33c4947a8784'
    
  },
  
  
  
    outputRoot: `./dist/${process.env.TARO_ENV}`,
  
  plugins: {
    babel: {
      
      
      
    }
  },
  weapp: {
    
    compile: {
      
    },
    
    module: {
      postcss: {
        autoprefixer: {
          
          
        },
        pxtransform: {
          
          
        },
        url: {
          
          
        },
        cssModules: {
          
          
        }
      }
    }
  },
  h5: {
    
    
    module: {
      postcss: {
        autoprefixer: {
          
          
        },
        pxtransform: {
          
          
        },
        url: {
          
          
        },
        cssModules: {
          
          
        }
      }
    }
  },
  rn: {
    
  }
}

module.exports.configFunc = function (merge) {
  if (process.env.NODE_ENV === 'development') {
    return merge({}, config, require('./dev'))
  }
  return merge({}, config, require('./prod'))
}

module.exports.configObj = config
