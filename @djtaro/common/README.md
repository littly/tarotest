### 介绍

@djtaro/common是到家taro项目公共库，提供到家基础业务能力，目前兼容小程序和h5业务。基础库包含登录、分享、埋点、跳转协议等功能。

### 安装
```
npm install --save @djtaro/common
```

### 使用说明
```javascript
// 引用
import {isLogin} from '@djtaro/common'

// 调用
isLogin().then(() => {
    ...
}).catch(err => {
    console.log('err==>', err)
})
```
